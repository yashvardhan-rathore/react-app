import { storiesOf } from '@storybook/react';
import * as React from 'react';
import FluidLayout from "./FluidLayout";
import NestedLayout from "./NestedLayout";


storiesOf("Layouts", module)
    .add("Fluid Grids",
    () => <FluidLayout />
    )
    .add("Nested Grids",
    () => <NestedLayout />
    );