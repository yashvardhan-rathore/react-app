import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const classes = {
  root: {
    flexGrow: 1
  },
  paper: {
    padding: 20,
    textAlign: "center",
    // color: theme.palette.text.secondary,
    fontFamily: "Roboto"
  }
};

export default () => {
  return (
    <div >
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper >xs=12</Paper>
        </Grid>
        <Grid item xs={12} sm={9}>
          <Paper >xs=12 sm=9</Paper>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Paper >xs=12 sm=6</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper >xs=6 sm=3</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper >xs=6 sm=3</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper >xs=6 sm=3</Paper>
        </Grid>
        <Grid item xs={6} sm={10}>
          <Paper>xs=6 sm=10</Paper>
        </Grid>
      </Grid>
    </div>
  );
}