import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const classes = {
  root: {
    flexGrow: 1
  },
  paper: {
    padding: 30,
    textAlign: "right"
  }
};

export default () => {
  function InnerGrid() {
    return (
      <React.Fragment>
        <Grid item xs={6}>
          <Paper >item</Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper >item</Paper>
        </Grid>
      </React.Fragment>
    );
  }

  return (
    <div >
      <Grid container spacing={1}>
        <Grid container item xs={12} spacing={3}>
          <InnerGrid />
        </Grid>
        <Grid container item xs={12} spacing={3}>
          <InnerGrid />
        </Grid>
        <Grid container item xs={12} spacing={3}>
          <InnerGrid />
        </Grid>
      </Grid>
    </div>
  );
}