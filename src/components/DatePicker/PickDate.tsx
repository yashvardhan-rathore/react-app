import React from 'react';
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import { value } from 'jsonpath';

export interface DatePickerInterface {
    dateFormat: string
    fromStartDate: string
    toStartaDte?: Date
    dateRange?: boolean
    isTimeReq?: boolean
    isSingleDate?: boolean
    onChange?: () => void

}
export default (props: DatePickerInterface) => {
    const { dateFormat, fromStartDate, toStartaDte, dateRange, isTimeReq, isSingleDate, onChange } = props;

    return (
        <div>

            {isSingleDate === true && (
                <div>
                    <label>From Date :</label>
                    <DatePicker
                        dateFormat={dateFormat}
                        selected={new Date()}
                        onChange={() => {
                        }
                        }
                    />
                </div>
            )
            }

            {dateRange === true && (
                <div>
                    <label>From Date :</label>
                    <DatePicker
                        dateFormat={dateFormat}
                        selected={new Date()}
                        onChange={() => {
                        }
                        }
                    />

                    <label>To Date :</label>
                    <DatePicker
                        dateFormat={dateFormat}
                        selected={new Date()}
                        onChange={() => {
                        }
                        }
                    />
                </div>
            )
            }
        </div>
    )
}
//yashvardhan
