import React from "react";
import { storiesOf } from "@storybook/react";
import DatePicker from "./PickDate";
storiesOf("DatePicker", module)
    .add("DatePicker",
        () => <DatePicker dateFormat="d-MM-yyyy" fromStartDate="" dateRange={false} isSingleDate={true} />
    )
    .add("DatePickerwithTime",
        () => <DatePicker dateFormat="d-MM-yyyy hh:mm:ss" fromStartDate="" dateRange={false} isSingleDate={true} />
    )
    .add("RangeDatePicker",
        () => <DatePicker dateFormat="d-MM-yyyy " fromStartDate="" dateRange={true} />
    )
    .add("RangeDatePickerwithTime",
        () => <DatePicker dateFormat="d-MM-yyyy hh:mm:ss" fromStartDate="" dateRange={true} />
    )

//yashvardhan