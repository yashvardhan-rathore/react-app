import React from "react";
import { storiesOf } from "@storybook/react";
import Button from "./Button";
import { withKnobs, text, boolean, number, color } from "@storybook/addon-knobs";

storiesOf("Button", module)
.addDecorator(withKnobs)
.add("Simple button",
() => <Button backgroundColor="white" type="submit" disabled={false} flag={true} name={text("name","Button")}/>
)

.add("different Color",
() => <Button backgroundColor={color ("color" , "red")} disabled={false} type="submit" flag={true} name={text("name","Button")}/>
)

.add("Disabled",
() => <Button backgroundColor="White" disabled={true} type="submit" flag={true} name={text("name","Button")}/>
)

.add("Button with onClick action",
() => <Button backgroundColor="White" disabled={false} type="submit" flag={true} onClick={() => {
alert("This button has an onClick action");
}} name={text("name","Button")} />
)

.add("Button with icon",
() => <Button backgroundColor="Red" disabled={false} type="submit"  flag={false} name={text("name","Add Todo")}/>
)
