import React from "react";
import { storiesOf } from "@storybook/react";
import Button from "./NormalButton";


export const buttonStoryProps = {disabled:false, name:"Button"};

export default storiesOf("NormalButton", module)

.add("NormalColorlessButton",
() => <Button {...buttonStoryProps} />
)


// .add("Simple button",
// () => <Button backgroundColor="white" disabled={false} flag={true} name={text("name","Button")}/>
// )

// .add("different Color",
// () => <Button backgroundColor={color ("color" , "red")} disabled={false} flag={true} name={text("name","Button")}/>
// )
