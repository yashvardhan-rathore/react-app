import React, { useEffect, useState, FormEvent } from 'react';

export interface IProps {
    backgroundColor?: string,
    disabled?:boolean,
    onClick?: (e: React.FormEvent<HTMLButtonElement>) => void,
    handleSubmit?:(e: React.FormEvent<HTMLButtonElement>) => void,
    flag?:boolean,
    name?: string,
    type?: string
}

export default (props: IProps) => {
    const {backgroundColor,disabled,onClick,flag,name,handleSubmit,type} = props;
    const [likes, setLikes] = useState(0);
    useEffect(() => {
        console.log('useeffect called');
    });
    if(flag===true)
    return (
      <div>
          Likes: {likes} <button style={{backgroundColor}} disabled={disabled} onClick={() => setLikes(likes + 1)}  >{name}</button>
      </div>
     )

     else
     return(
         <div>
             <button type="submit" style={{backgroundColor}} disabled={disabled} onClick={handleSubmit} > Refresh </button>
         </div>
     )
   
}
