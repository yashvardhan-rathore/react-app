type Todo = {
  text: string;
  complete: boolean;
};

type ToggleTodo = (selectedTodo: Todo) => void;

type AddTodo = (newTodo: string) => void;


const handleSubmit = (e: FormEvent<HTMLButtonElement>) => {
  e.preventDefault();
  addTodo(newTodo);
  setNewTodo("");
}

interface AddTodoFormProps {
  addTodo: AddTodo;
}