import React from "react";

interface Task{
    title: string;
}

interface TaskListProps{
    tasks: Task[];
}


const TaskList = ({tasks}: TaskListProps) => (<ul>
    {tasks.map((task, i) => {
        return <li key={i}>{task.title}</li>;
    })}
</ul>);

const tasks = [
    {title: 'task one'},
    {title: 'task 2'}
];

export default () => <div><TaskList tasks={tasks}/></div>